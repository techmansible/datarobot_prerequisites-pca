datatobot_prereqisites:

1. Create mandatory directory structure.
2. Create the soft link in linux for complying the Datarobot Inc. specific directory structure.
3. Provide sudo privileges.
4. Establish the SSH and passwordless auth between the App & Prediction servers.
5. Configure Limits, Keyrings and Selinux.
6. Create Private key and SSL certificate request
